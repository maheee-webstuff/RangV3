
require(['vendor/vue', 'vendor/vue-resource.min', 'components/main'], function (Vue, VueResource, VueInfiniteLoading, components) {

  Vue.use(VueResource);

  new Vue({
      el: '#app'
  });

});
