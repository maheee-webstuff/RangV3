
define(['vendor/vue', 'config'], function (Vue, CFG) {

  Vue.component('r-page', {
    template: `
      <r-thread v-for="thread in page.threads" v-bind:thread="thread">
      </r-thread>
    `,
    props: ['page'],
  });

  Vue.component('r-thread', {
    template: `
      <div class="thread">
        <div class="image">
          <a v-on:click="threadSelected" target="_blank">
            <img v-bind:src="imageSource" />
          </a>
        </div>
        <div class="text">
          <div v-if="title" class="title">
            {{title}}
          </div>
          {{{comment}}}
        </div>
        <div class="origlink">
          <a href="{{boardUrl}}" target="_blank"></a>
        </div>
      </div>
    `,
    props: ['thread'],
    data: function () {
      return {
        posts: {},
        imageSource: '',
        boardUrl: '',
        title: '',
        comment: '',
        info: {}
      };
    },
    methods: {
      threadSelected: function () {
        this.$dispatch('threadSelected', this.posts);
      },
    },
    ready: function () {
      this.$http.get(CFG.getThreadUrl(this.thread.no)).then((response) => {
          // success callback
          let data = JSON.parse(response.data);
          this.posts = data.posts;
          this.info = data.posts[0];
          this.title = this.info.sub;// + ' (' + this.info.name + ')';
          this.imageSource = CFG.getThumbnailUrl(this.info.tim);
          this.boardUrl = CFG.getBoardUrl(this.thread.no);
          this.comment = this.info.com;

      }, (response) => {
          // error callback
          //this.$dispatch('error', response);

          this.posts = {};
          this.info = {};
          this.title = 'deleted';
          this.imageSource = '';
          this.boardUrl = '';
          this.comment = '';
      });
    }
  });

});
