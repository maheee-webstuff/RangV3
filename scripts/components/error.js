
define(['vendor/vue', 'config'], function (Vue, CFG) {

  Vue.component('r-error', {
    template: `
      <div class="error" v-if="show">
        <div class="layer" v-on:click="hide"></div>
        <div class="content">
          <h1>An error occured!</h2>
          <br />
          <div>{{message}}</div>
        </div>
      </div>
    `,
    data: function () {
      return {
        show: false,
        message: '',
      };
    },
    methods: {
      hide: function () {
        this.show = false;
      },
    },
    events: {
      error: function (message) {
        this.show = true;
        this.message = 'Try again or reload the page!';
      }
    },
  });

});
