
define(['vendor/vue', 'config'], function (Vue, CFG) {

  Vue.component('r-details', {
    template: `
      <div class="posts" v-if="show">
        <div class="layer"
            v-on:click="hide">
        </div>
        <div class="content"
            onmousewheel="function() {ev.stopPropagation();ev.preventDefault();ev.returnValue = false;return false;}">
          <a href="{{boardLink}}" target="_blank">{{boardLink}}</a>
          <br />
          <r-detail v-for="post in posts" v-bind:post="post"></r-detail>
        </div>
      </div>
    `,
    data: function () {
      return {
        show: false,
        posts: [],
        boardLink: '',
      };
    },
    methods: {
      hide: function () {
        this.show = false;
      },
    },
    events: {
      threadSelected: function (data) {
        this.posts = data;
        this.show = true;
        this.boardLink = CFG.getBoardUrl(data[0].no);
      }
    },
  });

  Vue.component('r-detail', {
    template: `
      <div class="post">
        <div class="meta">
          {{meta}}
        </div>
        <div v-if="imageSource" class="image">
          <a href="{{imageLink}}" target="_blank">
            <img v-bind:src="imageSource" />
          </a>
        </div>
        <div class="text">
          {{{comment}}}
        </div>
      </div>
    `,
    props: ['post'],
    data: function () {
      return {
        comment: '',
        imageSource: '',
        imageLink: '',
        meta: ''
      };
    },
    ready: function () {
      if (this.post.tim) {
        this.imageSource = CFG.getThumbnailUrl(this.post.tim);
        this.imageLink = CFG.getImageUrl(this.post.tim, this.post.ext);
      }
      this.comment = this.post.com;
      this.meta = new Date(this.post.time * 1000);
    }
  });

});
