
define(['vendor/vue', 'config', 'components/page', 'components/details', 'components/error'], function (Vue, CFG, CompPage, CompDetails, CompError) {

  Vue.component('r-main', {
    template: `
      <div>
        <div>
          <h1>{{title}}</h1>
          <select v-model="selectedBoard">
            <option v-for="board in boards" value="{{$index}}">{{board.group}} - {{board.title}}</option>
          </select>
        </div>
        <r-page v-for="page in data" v-bind:page="page"></r-page>
        <r-details></r-details>
        <r-error></r-error>
      </div>
    `,
    data: function () {
      return {
        fullData: [],
        data: [],
        title: '',
        boards: [],
        selectedBoard: '',
      };
    },
    watch: {
      selectedBoard: function () {
        CFG.setBoard(this.selectedBoard);
        this.$dispatch('update');
      }
    },
    events: {
      update: function () {
        var board = CFG.getBoard();
        this.title = board.group + ' - ' + board.title;
        this.$http.get(CFG.getThreadsUrl()).then((response) => {
            this.fullData = JSON.parse(response.data);
            this.data = [];
            if (this.fullData.length >= 1) {
              this.data.push(this.fullData[0]);
            }
        }, (response) => {
            // error callback
            this.$dispatch('error', response);
        });
      },
      threadSelected: function (data) {
        this.$broadcast('threadSelected', data);
      },
      error: function (data) {
        this.$broadcast('error', data);
      },
    },
    ready: function () {
      this.boards = CFG.getBoards();
      this.selectedBoard = CFG.getBoardNo();

      var component = this;
      var scrollOn = function (event) {
        if (Math.round(this.scrollMaxY) === Math.round(this.scrollY)) {
          if (component.fullData.length > component.data.length) {
            component.data.push(component.fullData[component.data.length]);
          }
        }
      };
      window.addEventListener('scroll', scrollOn);
      setTimeout(scrollOn, 100);
    },
  });

});
