
define([], function () {

  var API_BASE = 'http://a.4cdn.org/';
  var IMG_BASE = 'http://i.4cdn.org/';
  var BOARD_BASE = 'http://boards.4chan.org/';
  var BOARD_NO = 0;
  var BOARDS = [
    {code: 'a', group: 'Japanese Culture', title: 'Anime &amp; Manga'},
    {code: 'c', group: 'Japanese Culture', title: 'Anime/Cute'},
    {code: 'w', group: 'Japanese Culture', title: 'Anime/Wallpapers'},
    {code: 'm', group: 'Japanese Culture', title: 'Mecha'},
    {code: 'cgl', group: 'Japanese Culture', title: 'Cosplay &amp; EGL'},
    {code: 'cm', group: 'Japanese Culture', title: 'Cute/Male'},
    {code: 'f', group: 'Japanese Culture', title: 'Flash'},
    {code: 'n', group: 'Japanese Culture', title: 'Transportation'},
    {code: 'jp', group: 'Japanese Culture', title: 'Otaku Culture'},
    {code: 'v', group: 'Video Games', title: 'Video Games'},
    {code: 'vg', group: 'Video Games', title: 'Video Game Generals'},
    {code: 'vp', group: 'Video Games', title: 'Pokémon'},
    {code: 'vr', group: 'Video Games', title: 'Retro Games'},
    {code: 'co', group: 'Interests', title: 'Comics &amp; Cartoons'},
    {code: 'g', group: 'Interests', title: 'Technology'},
    {code: 'tv', group: 'Interests', title: 'Television &amp; Film'},
    {code: 'k', group: 'Interests', title: 'Weapons'},
    {code: 'o', group: 'Interests', title: 'Auto'},
    {code: 'an', group: 'Interests', title: 'Animals &amp; Nature'},
    {code: 'tg', group: 'Interests', title: 'Traditional Games'},
    {code: 'sp', group: 'Interests', title: 'Sports'},
    {code: 'asp', group: 'Interests', title: 'Alternative Sports'},
    {code: 'sci', group: 'Interests', title: 'Science &amp; Math'},
    {code: 'his', group: 'Interests', title: 'History &amp; Humanities'},
    {code: 'int', group: 'Interests', title: 'International'},
    {code: 'out', group: 'Interests', title: 'Outdoors'},
    {code: 'toy', group: 'Interests', title: 'Toys'},
    {code: 'i', group: 'Creative', title: 'Oekaki'},
    {code: 'po', group: 'Creative', title: 'Papercraft &amp; Origami'},
    {code: 'p', group: 'Creative', title: 'Photography'},
    {code: 'ck', group: 'Creative', title: 'Food &amp; Cooking'},
    {code: 'ic', group: 'Creative', title: 'Artwork/Critique'},
    {code: 'wg', group: 'Creative', title: 'Wallpapers/General'},
    {code: 'lit', group: 'Creative', title: 'Literature'},
    {code: 'mu', group: 'Creative', title: 'Music'},
    {code: 'fa', group: 'Creative', title: 'Fashion'},
    {code: '3', group: 'Creative', title: '3DCG'},
    {code: 'gd', group: 'Creative', title: 'Graphic Design'},
    {code: 'diy', group: 'Creative', title: 'Do-It-Yourself'},
    {code: 'wsg', group: 'Creative', title: 'Worksafe GIF'},
    {code: 'qst', group: 'Creative', title: 'Quests'},
    {code: 'biz', group: 'Other', title: 'Business &amp; Finance'},
    {code: 'trv', group: 'Other', title: 'Travel'},
    {code: 'fit', group: 'Other', title: 'Fitness'},
    {code: 'x', group: 'Other', title: 'Paranormal'},
    {code: 'adv', group: 'Other', title: 'Advice'},
    {code: 'lgbt', group: 'Other', title: 'LGBT'},
    {code: 'mlp', group: 'Other', title: 'Pony'},
    {code: 'news', group: 'Other', title: 'Current News'},
    {code: 'wsr', group: 'Other', title: 'Worksafe Requests'},
    {code: 'vip', group: 'Other', title: 'Very Important Posts'},
    {code: 'b', group: 'NSFW Misc.', title: 'Random'},
    {code: 'r9k', group: 'NSFW Misc.', title: 'ROBOT9001'},
    {code: 'pol', group: 'NSFW Misc.', title: 'Politically Incorrect'},
    {code: 'bant', group: 'NSFW Misc.', title: 'International/Random'},
    {code: 'soc', group: 'NSFW Misc.', title: 'Cams &amp; Meetups'},
    {code: 's4s', group: 'NSFW Misc.', title: 'Shit 4chan Says'},
    {code: 's', group: 'NSFW Adult', title: 'Sexy Beautiful Women'},
    {code: 'hc', group: 'NSFW Adult', title: 'Hardcore'},
    {code: 'hm', group: 'NSFW Adult', title: 'Handsome Men'},
    {code: 'h', group: 'NSFW Adult', title: 'Hentai'},
    {code: 'e', group: 'NSFW Adult', title: 'Ecchi'},
    {code: 'u', group: 'NSFW Adult', title: 'Yuri'},
    {code: 'd', group: 'NSFW Adult', title: 'Hentai/Alternative'},
    {code: 'y', group: 'NSFW Adult', title: 'Yaoi'},
    {code: 't', group: 'NSFW Adult', title: 'Torrents'},
    {code: 'hr', group: 'NSFW Adult', title: 'High Resolution'},
    {code: 'gif', group: 'NSFW Adult', title: 'Adult GIF'},
    {code: 'aco', group: 'NSFW Adult', title: 'Adult Cartoons'},
    {code: 'r', group: 'NSFW Adult', title: 'Adult Requests'},
  ];

  if (window.localStorage) {
    boardCode = window.localStorage.getItem('selectedBoard');
    if (boardCode) {
      for (var i in BOARDS) {
        if (BOARDS[i].code === boardCode) {
          BOARD_NO = i;
          break;
        }
      }
    }
  }

  return {
    getBoards: function () {
      return BOARDS;
    },
    getBoard: function () {
      return BOARDS[BOARD_NO];
    },
    getBoardNo: function () {
      return BOARD_NO;
    },
    setBoard: function (boardNo) {
      BOARD_NO = boardNo;
      // store selected board in localStorage
      if (window.localStorage) {
        window.localStorage.setItem('selectedBoard', BOARDS[BOARD_NO].code);
      }
    },
    getThreadsUrl: function () {
      var board = BOARDS[BOARD_NO].code;
      return API_BASE + board + '/threads.json';
    },
    getThreadUrl: function (no) {
      var board = BOARDS[BOARD_NO].code;
      return API_BASE + board + '/thread/' + no + '.json';
    },
    getThumbnailUrl: function (id) {
      var board = BOARDS[BOARD_NO].code;
      return IMG_BASE + board + '/' + id + 's.jpg';
    },
    getImageUrl: function (id, ext) {
      var board = BOARDS[BOARD_NO].code;
      return IMG_BASE + board + '/' + id + '' + ext;
    },
    getBoardUrl: function (no) {
      var board = BOARDS[BOARD_NO].code;
      return BOARD_BASE + board + '/thread/' + no + '/';
    }
  };

});
